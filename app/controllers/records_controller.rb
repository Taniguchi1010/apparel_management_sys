class RecordsController < ApplicationController

  def create
    @merchandise_management = MerchandiseManagement.find(params[:merchandise_management_id])
    @record = @merchandise_management.records.create(record_params)
    redirect_to merchandise_management_path(@merchandise_management.id)
  end
  
  def destroy
    @record = Record.find(params[:id])
    @record.destroy
    redirect_to merchandise_management_path(params[:merchandise_management_id])
  end

  private
    def record_params
      params[:record].permit(:in, :q)
    end
end
