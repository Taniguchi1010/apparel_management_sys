class MerchandiseManagementsController < ApplicationController
  before_action :set_merchandise_management, only: [:show, :edit, :update, :destroy]

  def index
    @merchandise_managements = MerchandiseManagement.search(params[:q])
  end

  def show
  end

  def new
    @merchandise_management = MerchandiseManagement.new
  end

  def edit
  end

  def create
    @merchandise_management = MerchandiseManagement.new(merchandise_management_params)
    respond_to do |format|
      if @merchandise_management.save
        format.html { redirect_to @merchandise_management, notice: 'Merchandise management was successfully created.' }
        format.json { render :show, status: :created, location: @merchandise_management }
      else
        format.html { render :new }
        format.json { render json: @merchandise_management.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @merchandise_management.update(merchandise_management_params)
        format.html { redirect_to @merchandise_management, notice: 'Merchandise management was successfully updated.' }
        format.json { render :show, status: :ok, location: @merchandise_management }
      else
        format.html { render :edit }
        format.json { render json: @merchandise_management.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @merchandise_management.destroy
    respond_to do |format|
      format.html { redirect_to merchandise_managements_url, notice: 'Merchandise management was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_merchandise_management
      @merchandise_management = MerchandiseManagement.find(params[:id])
    end

    def merchandise_management_params
      params.require(:merchandise_management).permit(:picture, :name, :size, :color, :number, :maker, :price)
    end
end
