json.array!(@merchandise_managements) do |merchandise_management|
  json.extract! merchandise_management, :id, :name, :size, :color, :number, :maker, :quantity, :price
  json.url merchandise_management_url(merchandise_management, format: :json)
end
