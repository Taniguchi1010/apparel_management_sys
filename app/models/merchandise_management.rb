class MerchandiseManagement < ActiveRecord::Base
  belongs_to :user
  default_scope -> { order(created_at: :desc) }
  mount_uploader :picture, PictureUploader

  validates :number, presence: true
  validates_uniqueness_of :number, :message => "は既に登録されています"

  has_many:records

  scope :search, ->(keyword = nil) do
    if keyword.present?
      sql = <<-EOS
        `name`    LIKE :keyword OR
        `size`    LIKE :keyword OR
        `color`   LIKE :keyword OR
        `number`  LIKE :keyword OR
        `maker`   LIKE :keyword
      EOS
      where(sql, keyword: "%#{keyword}%")
    end
  end

  def quantity
    self.records.sum(:q)
  end
end


