class Record < ActiveRecord::Base
  belongs_to :merchandise_management
  validates :in, presence: true
end
