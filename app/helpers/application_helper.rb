module ApplicationHelper
  def number_image_tag(merchandise_management)
    image_tag "#{merchandise_management.number}.jpg"
  end
end
