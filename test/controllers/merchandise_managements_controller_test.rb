require 'test_helper'

class MerchandiseManagementsControllerTest < ActionController::TestCase
  setup do
    @merchandise_management = merchandise_managements(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:merchandise_managements)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create merchandise_management" do
    assert_difference('MerchandiseManagement.count') do
      post :create, merchandise_management: { color: @merchandise_management.color, maker: @merchandise_management.maker, name: @merchandise_management.name, number: @merchandise_management.number, price: @merchandise_management.price, size: @merchandise_management.size }
    end

    assert_redirected_to merchandise_management_path(assigns(:merchandise_management))
  end

  test "should show merchandise_management" do
    get :show, id: @merchandise_management
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @merchandise_management
    assert_response :success
  end

  test "should update merchandise_management" do
    patch :update, id: @merchandise_management, merchandise_management: { color: @merchandise_management.color, maker: @merchandise_management.maker, name: @merchandise_management.name, number: @merchandise_management.number, price: @merchandise_management.price, size: @merchandise_management.size }
    assert_redirected_to merchandise_management_path(assigns(:merchandise_management))
  end

  test "should destroy merchandise_management" do
    assert_difference('MerchandiseManagement.count', -1) do
      delete :destroy, id: @merchandise_management
    end

    assert_redirected_to merchandise_managements_path
  end
end
