Rails.application.routes.draw do

  root 'merchandise_managements#index'
  resources :merchandise_managements do
    resources :records, only: [:create, :destroy]
  end

end

