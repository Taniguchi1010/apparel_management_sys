class RemoveQuantityFromMerchandiseManagements < ActiveRecord::Migration
  def change
    remove_column :merchandise_managements, :quantity
  end
end
