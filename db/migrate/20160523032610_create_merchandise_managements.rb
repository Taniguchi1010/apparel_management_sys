class CreateMerchandiseManagements < ActiveRecord::Migration
  def change
    create_table :merchandise_managements do |t|
      t.string :name
      t.string :size
      t.string :color
      t.string :number
      t.string :maker
      t.integer :quantity
      t.integer :price

      t.timestamps null: false
    end
  end
end
