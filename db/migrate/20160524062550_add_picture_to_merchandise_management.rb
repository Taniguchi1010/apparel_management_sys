class AddPictureToMerchandiseManagement < ActiveRecord::Migration
  def change
    add_column :merchandise_managements, :picture, :string
  end
end
