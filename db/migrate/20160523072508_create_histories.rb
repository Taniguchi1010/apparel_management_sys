class CreateHistories < ActiveRecord::Migration
  def change
    create_table :histories do |t|
      t.references :merchandise_managements, index: true, foreign_key: true
      t.date :stocked_at
      t.integer :quantity

      t.timestamps null: false
    end
  end
end
