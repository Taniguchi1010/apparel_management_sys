class CreateRecords < ActiveRecord::Migration
  def change
    create_table :records do |t|
      t.date :in
      t.integer :q
      t.references :merchandise_management, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
