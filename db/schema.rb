# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160526053918) do

  create_table "histories", force: :cascade do |t|
    t.integer  "merchandise_managements_id"
    t.date     "stocked_at"
    t.integer  "quantity"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "histories", ["merchandise_managements_id"], name: "index_histories_on_merchandise_managements_id"

  create_table "merchandise_managements", force: :cascade do |t|
    t.string   "name"
    t.string   "size"
    t.string   "color"
    t.string   "number"
    t.string   "maker"
    t.integer  "price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "picture"
  end

  create_table "records", force: :cascade do |t|
    t.date     "in"
    t.integer  "q"
    t.integer  "merchandise_management_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "records", ["merchandise_management_id"], name: "index_records_on_merchandise_management_id"

end
